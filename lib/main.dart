// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:miracle/UI/Screens/auth.dart';
import 'package:miracle/UI/Screens/start.dart';

import 'UI/Screens/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/login':(c) =>AuthScreen(
          buttonText: 'Login',
          questionText: 'Don\'t have an account?',
          route: '/register',
        ),
        '/register':(c) =>AuthScreen(
          buttonText: 'Register',
          questionText: 'Already have an account?',
          route: '/login',
        ),
        '/home' : (c) =>Home()
      },
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          elevation: 0,
            centerTitle: true,

          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(AppBar().preferredSize.height*0.5)
            )
          ),
          backgroundColor: Colors.deepPurple
        )
      ),
      home: StartScreen()
    );
  }
}
