// ignore_for_file: prefer_const_constructors, curly_braces_in_flow_control_structures, prefer_const_literals_to_create_immutables

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:miracle/UI/Screens/Section.dart';
import 'package:carousel_slider/carousel_controller.dart';


class Home extends StatelessWidget {

  static final List<String> sections = [
    for(int i = 0 ; i < 8 ; i++)
    'section ${i+1}',
  ];

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Miracle'),
        actions: [
          IconButton(
              onPressed: (){},
              icon: Icon(Icons.search)
          )
        ],
      ),
      body: CarouselSlider(
          items: [
            Image.network('https://media.istockphoto.com/photos/child-girl-cotton-bright-summer-clothes-set-collage-isolated-picture-id508756496?k=20&m=508756496&s=612x612&w=0&h=zN04iW81RatIZ1AaxFfegFyp2kvfxpyYS7ZRGOVuSgs='),
            Image.network('https://media.istockphoto.com/photos/cute-little-girl-having-fun-time-in-the-nature-picture-id1294345604?b=1&k=20&m=1294345604&s=170667a&w=0&h=MIdcDRPMnHDqX8E0_swEtZA69GVvnaaQF60VgAng9mY='),
            Image.network('https://9b16f79ca967fd0708d1-2713572fef44aa49ec323e813b06d2d9.ssl.cf2.rackcdn.com/1140x_a10-7_cTC/PreciousAndPosh-2-jpg-1628502403.jpg'),
          ],
          options: CarouselOptions(
            height: size.height*0.25,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            autoPlayAnimationDuration: Duration(milliseconds: 1200),
          )
      ),
      drawer: Drawer(
        child: Column(
          children: [
            DrawerHeader(child: Container()),
            Expanded(
                child: ListView.builder(
                  itemCount: sections.length,
                    itemBuilder: (c,i) => Card(
                      child: ListTile(
                        title: Text(sections[i]),
                        leading: Icon(Icons.star_border),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (c) => SectionScreen(sections[i])));
                        },
                      ),
                    ),
                ),
            )
          ],
        ),
      ),
    );
  }
}
