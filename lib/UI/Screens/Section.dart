// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

class SectionScreen extends StatelessWidget {

  final String title;

  SectionScreen(this.title);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView.builder(
        itemCount: 10,
        itemBuilder: (context,i){
          var size = MediaQuery.of(context).size;
          return Container(
            width: double.infinity,
            height: size.height*0.2,
            margin: EdgeInsets.all(size.width*0.025),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(size.width*0.025),
                color: Colors.grey.withOpacity(0.3)
            ),
            child: Row(
              children: [
                Expanded(child: Image.network('https://static.zajo.net/content/mediagallery/zajo_dcat/image/product/types/X/9088.png')),
                Expanded(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.all(size.width*0.025),
                      child: ListTile(
                        title: Text('T-Shirt <Example>'),
                        subtitle: Row(
                          children: [
                            Icon(Icons.price_check_rounded),
                            Text(' 220 \$'),
                          ],
                        ),
                      ),
                    )
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
