import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:miracle/UI/Elements/Buttons.dart';


class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.deepPurple,
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: size.height*0.01),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(),
            Container(
                child: Image.asset('svg/c1.png',),
              height: size.height*0.5,
            ),
            StartButton(
              text: 'Get Started',
              color: Colors.white,
              textColor: Colors.purpleAccent,
              onTap: (){
                Navigator.pushReplacementNamed(context, '/login');
              },
            )
          ],
        ),
      ),
    );
  }
}
