import 'package:flutter/material.dart';
import 'package:miracle/UI/Elements/Buttons.dart';
import 'package:miracle/UI/Elements/TextFields.dart';

class AuthScreen extends StatelessWidget {

  final String buttonText;
  final String questionText;
  final String route;


  AuthScreen({required this.buttonText,required this.questionText,required this.route});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: size.height*0.475,
              decoration: BoxDecoration(
                color: Colors.purple,
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(size.width*0.1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.all(size.width*0.05),
                    margin: EdgeInsets.all(size.width*0.05),
                    height: size.height*0.1,
                    child:const FittedBox(
                      child: Text('Hello!',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                  ),
                  Container(
                    child: Image.asset('svg/c1.png',),
                    height: size.height*0.25,
                  ),
                ],
              ),
            ),
            AuthField(hint: 'Enter your Email',),
            AuthField(hint: 'Enter your Password',isPass: true,),
            StartButton(text: buttonText,textColor: Colors.white,color: Colors.purple,
            onTap: (){Navigator.pushReplacementNamed(context, '/home');},
            ),
            InkWell(
                child: Text(questionText),
              onTap: (){
                  Navigator.pushReplacementNamed(context, route);
              },
            ),
          ],
        ),
      ),
    );
  }
}
