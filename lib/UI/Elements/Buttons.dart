import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StartButton extends StatelessWidget {

  final String text;
  final Color? textColor;
  final Color? color;
  final GestureTapCallback? onTap;

  StartButton({required this.text, this.textColor, this.color,this.onTap});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: size.height*0.08,
        margin: EdgeInsets.all(size.width*0.05),
        padding: EdgeInsets.all(size.width*0.035),
        child: FittedBox(
          child: Text(text,style: TextStyle(
            color: textColor
          ),),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size.height*0.08),
          color: color
        ),
      ),
    );
  }
}
