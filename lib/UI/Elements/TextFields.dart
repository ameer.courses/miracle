// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class AuthField extends StatelessWidget {

  final String? hint;
  final bool isPass;

  AuthField({this.hint,this.isPass = false});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height*0.08,
      margin: EdgeInsets.all(size.width*0.05),
      padding: EdgeInsets.all(size.width*0.025),
      child: TextField(
        decoration: InputDecoration(
          hintText: hint,
          border: InputBorder.none
        ),
        obscureText: isPass,
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size.height*0.08),
          color: Colors.grey.withOpacity(0.2)
      ),
    );
  }
}
